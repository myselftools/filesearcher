﻿using ArgsParser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FileSearcher
{
    public class Options : ArgsOptionsBase
    {
        /* Скрыть окно
         * Скрыть вывод
         * Проверять все типы дисков
         * Маска для файла
         * Путь сохранения
         * Выключение логирования
         * Имя файла логов
         * Список доступных дисков
         * Список исключенных дисков
         * Список доступных форматов
         * Список исключенных форматов
         */

        // -noout -alldrives -mask *.d* -spath My -adrives F -aformats .pdf .doc 

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [MethodParamAttribute("h", "Hide", Priority=0, Description="Скрывает окно консольного приложения")]
        public void HideWindow()
        {
            IntPtr h = Process.GetCurrentProcess().MainWindowHandle;
            ShowWindow(h, 0);
        }

        [PropertyParamAttribute("noout", "Nooutput", DefaultValue=false, Description="Скрывает вывод выполняемых действий")]
        public bool HideOutput { get; set; }

        [PropertyParamAttribute("alldrives", "AllDriversTypes", DefaultValue = false, Description = "Если указан, то проевряются все типы дисков")]
        public bool AllDriveTypes { get; set; }

        [PropertyParamAttribute("mask", "FileSearchPattern", DefaultValue = "*.*", Description = "Маска для поиска файлов")]
        public string Mask { get; set; }

        [PropertyParamAttribute("spath", "SavePath", DefaultValue="\\MyFiles", Description = "Путь сохранения файлов. Рассчитывается от корневой дирректории")]
        public string SavePath { get; set; }

        [PropertyParamAttribute("logoff", "DisableLoging", DefaultValue = false, Description = "Выключает логирование ошибок")]
        public bool LogOff { get; set; }

        [PropertyParamAttribute("logfile", "LogFileName", DefaultValue = "Log.txt", Description = "Имя файла логов")]
        public string LogFileName { get; set; }

        [PropertyParamAttribute("adrives", "AvalibleDrives", Description="Список дисков для проверки")]
        public string[] AvalibleDrives { get; set; }

        [PropertyParamAttribute("edrives", "ExcludeDrives", Description = "Список дисков исключенных из проверки")]
        public string[] ExcludeDrives { get; set; }

        [PropertyParamAttribute("aformats", "AvalibleFormats", Description = "Список искомых форматов")]
        public string[] AvalibleFormats { get; set; }

        [PropertyParamAttribute("eformats", "ExcludeFormats", Description = "Список форматов исключенных из выборки")]
        public string[] ExcludeFormats { get; set; }

        [PropertyParamAttribute("?", "Help", Description="Описание параметров", DefaultValue=false)]
        public bool ShowHelp { get; set; }
    }

    class Program
    {
        static Options options = null;

        static string[] UpperCollection(string[] collection)
        {
            return collection != null ? collection.ToList().ConvertAll<string>(x => x.ToUpperInvariant()).ToArray() : null;
        }

        static void NormalizeCollections(Options options)
        {
            options.AvalibleDrives = UpperCollection(options.AvalibleDrives);
            options.ExcludeDrives = UpperCollection(options.ExcludeDrives);
            options.AvalibleFormats = UpperCollection(options.AvalibleFormats);
            options.ExcludeFormats = UpperCollection(options.ExcludeFormats);
        }

        static void WriteConsole(string value, bool write)
        {
            if (write) Console.WriteLine(value);
            Debug.WriteLine(value);
        }

        static void WriteLog(string value)
        {
            if (options.LogOff) return;

            try
            {
                File.AppendAllLines(options.LogFileName, new string[] { value });
                WriteConsole(value, !options.HideOutput);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[ERROR LOG]: {0}", ex.ToString());
            }

        }

        static List<FileInfo> GetFilesRec(DirectoryInfo dir, string mask)
        {
            List<FileInfo> files = new List<FileInfo>();
            try
            {// Тут бывают ошибки
                files.AddRange(dir.GetFiles(mask, SearchOption.TopDirectoryOnly).ToList());
                foreach (var d in dir.GetDirectories())
                {
                    files.AddRange(GetFilesRec(d, mask));
                }
            }
            catch (Exception ex)
            {
                File.AppendAllLines("log.txt", new string[] { ex.ToString() });
                WriteLog(ex.ToString());
            }

            return files;
        }

        static void Main(string[] args)
        {
            options = ArgsManager.ParseArgs<Options>(args);

            if (options.ShowHelp)
            {
                Console.WriteLine(options.GetParamHelp());
                Console.ReadLine();
                return;
            }

            NormalizeCollections(options);            

            var currDir = new DirectoryInfo(Environment.CurrentDirectory);

            var drives = DriveInfo.GetDrives().Where(x => x.DriveType == DriveType.Removable
                                                       && x.IsReady == true
                                                       && x.RootDirectory != currDir.Root).ToList();

            if (options.AllDriveTypes)
            {
                var localDrives = Directory.GetLogicalDrives().ToList().ConvertAll<DriveInfo>((x) => { return new DriveInfo(x); });

                drives.AddRange(localDrives);
            }

            if (options.AvalibleDrives != null)
                drives = drives.Where(x => options.AvalibleDrives.Contains(x.Name.Substring(0, 1))).ToList();

            if (options.ExcludeDrives != null)
                drives = drives.Where(x => !options.ExcludeDrives.Contains(x.Name.Substring(0, 1))).ToList();

            foreach (var drive in drives)
            {
                WriteConsole(String.Format("Drive:> {0}",drive.Name), !options.HideOutput);

                var files = GetFilesRec(drive.RootDirectory, options.Mask);

                WriteConsole(String.Format("Found {0} files", files.Count), !options.HideOutput);

                if (options.AvalibleFormats != null)
                    files = files.Where(x => options.AvalibleFormats.Contains(x.Extension.ToUpper())).ToList();

                if (options.ExcludeFormats != null)
                    files = files.Where(x => !options.ExcludeFormats.Contains(x.Extension.ToUpper())).ToList();

                WriteConsole(String.Format("Filtered {0} files", files.Count), !options.HideOutput);
                WriteConsole("Copying...", !options.HideOutput);

                foreach (var file in files)
                {
                    var fname = currDir.Root + options.SavePath + "\\" + drive.Name.Substring(0, 1) + "\\" + file.Name;
                    var fileInfo = new FileInfo(fname);
                    try
                    {
                        if (!fileInfo.Directory.Exists) Directory.CreateDirectory(fileInfo.Directory.FullName);

                        file.CopyTo(fname, true);
                        WriteConsole(String.Format("   {0} > {1}", file.FullName, fileInfo.FullName), !options.HideOutput);
                    }
                    catch (Exception ex)
                    {
                        WriteLog(String.Format("   ERROR ON {0} : {1}", file.FullName, ex.Message));   
                    }
                }
            }
        }
    }
}
